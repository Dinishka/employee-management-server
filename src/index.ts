import cors from 'cors';
import dotenv from 'dotenv';
import type { ErrorRequestHandler } from 'express';
import express from 'express';
import { ValidationError } from 'express-validation';
import jwt from 'jsonwebtoken';
import auth from './api/auth';
import department from './api/department';
import designation from './api/designation';
import employee from './api/employee';

const app = express();

// enable cors (should change in production)
app.use(cors());

//parse json in requests
app.use(express.json());

// load environment variables
dotenv.config();

// auth api
app.use('/api/auth', auth);

// authentication middleware
app.use((req, res, next) => {
   // check if auth header is available
   const authHeader = req.headers.authorization;
   if (!authHeader) return res.sendStatus(401);

   // check if token is available
   const token = authHeader.split(' ')[1];
   if (!token) return res.sendStatus(401);

   // verify the token
   jwt.verify(token, process.env.ACCESS_TOKEN_SECRET as jwt.Secret, (err, _payload) => {
      if (err) return res.status(401).json({ message: err.message });
      next();
   });
});

// employee api
app.use('/api/employee', employee);

// department api
app.use('/api/department', department);

// designation api
app.use('/api/designation', designation);


// global error handler
const errorHandler: ErrorRequestHandler = (err, _req, res, _next) => {
   if (err instanceof ValidationError) {
      return res.status(err.statusCode).json({ message: 'Validation Error!', error: err });
   }

   return res.status(500).json({ message: 'Server Error!', error: err });
}

app.use(errorHandler);

const port = 5000;
app.listen(port, () => {
   console.log(`Server started at port ${port}`);
});