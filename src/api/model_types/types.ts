import type { RowDataPacket } from 'mysql2';

interface Employee extends RowDataPacket {
   id: number;
   first_name: string;
   last_name: string;
   birthday: string;
   addresses: string[];
   contact_numbers: string[];
   nic: string;
   designation_id: number;
   designation: string;
   department_id: string;
   department_name: string;
}

interface User extends RowDataPacket {
   id: number;
   username: string;
   password: string;
}

export { Employee, User };