import { Router } from 'express';
import { validate } from 'express-validation';
import { promisePool } from '../config/db-config';
import { createDesignationValidator } from './request_validations/designation-validator';

const router = Router();

router.post('/', validate(createDesignationValidator, {}, {}), async (req, res, next) => {
   const { designation, department_id } = req.body;

   const query = 'INSERT INTO `designation` (`designation`, `department_id`) VALUES (?, ?)';
   const placeholderValues = [designation, department_id]

   try {
      await promisePool.execute(query, placeholderValues);
      res.json({ message: 'Successfully created a new designation' });
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

export default router;