import { Router } from 'express';
import { validate } from 'express-validation';
import type { ResultSetHeader } from 'mysql2';
import { promisePool } from '../config/db-config';
import type { Employee } from './model_types/types';
import { createEmployeeValidator, getEmployeeValidator, updateEmployeeValidator } from './request_validations/employee-validator';

const router = Router();

router.get('/', async (_, res, next) => {
   const query = 'SELECT e.id, e.first_name, e.last_name, e.birthday, e.addresses, e.contact_numbers, e.nic, d.id AS designation_id, d.designation, dp.id AS department_id, dp.department_name FROM employee e INNER JOIN designation d ON e.designation_id = d.id INNER JOIN department dp ON d.department_id = dp.id ORDER BY e.id';

   try {
      const [rows] = await promisePool.execute(query);
      res.json(rows);
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

router.get('/:id', validate(getEmployeeValidator, {}, {}), async (req, res, next) => {
   const { id } = req.params;

   const query = 'SELECT e.id, e.first_name, e.last_name, e.birthday, e.addresses, e.contact_numbers, e.nic, d.id AS designation_id, d.designation, dp.id AS department_id, dp.department_name FROM employee e INNER JOIN designation d ON e.designation_id = d.id INNER JOIN department dp ON d.department_id = dp.id WHERE e.id = ?';

   try {
      const [rows] = await promisePool.execute<Employee[]>(query, [id]);
      const employee = rows[0];
      res.json(employee);
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

router.post('/', validate(createEmployeeValidator, {}, {}), async (req, res, next) => {
   const { first_name, last_name, birthday, addresses, contact_numbers, nic, designation_id } = req.body;

   // Check if another employee with the same NIC is available
   const validationQuery = 'SELECT * FROM `employee` WHERE `nic` = ?';
   try {
      const [rows] = await promisePool.execute<Employee[]>(validationQuery, [nic]);
      // If available send error
      if (rows[0]) {
         return res.status(400).json({ message: "An employee with this NIC is already available." });
      }

      // Else insert an new employee
      const query = 'INSERT INTO `employee` (`first_name`, `last_name`, `birthday`, `addresses`, `contact_numbers`, `nic`, `designation_id`) VALUES (?, ?, ?, ?, ?, ?, ?)';
      const placeholderValues = [first_name, last_name, birthday, JSON.stringify(addresses), JSON.stringify(contact_numbers), nic, designation_id]

      const [result] = await promisePool.execute<ResultSetHeader>(query, placeholderValues);
      res.json({ message: `Successfully created a new employee with id ${result.insertId}` });
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

router.put('/:id', validate(updateEmployeeValidator, {}, {}), async (req, res, next) => {
   const { id } = req.params;
   const { first_name, last_name, birthday, addresses, contact_numbers, nic, designation_id } = req.body;

   const query = 'UPDATE `employee` SET `first_name` = ?, `last_name` = ?, `birthday` = ?, `addresses` = ?, `contact_numbers` = ?, `nic` = ?, `designation_id` = ? WHERE `id` = ?';
   const placeholderValues = [first_name, last_name, birthday, JSON.stringify(addresses), JSON.stringify(contact_numbers), nic, designation_id, id]

   try {
      await promisePool.execute(query, placeholderValues);
      res.json({ message: `Successfully updated the employee` });
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

router.delete('/:id', validate(getEmployeeValidator, {}, {}), async (req, res, next) => {
   const { id } = req.params;

   const query = 'DELETE FROM `employee` WHERE id=?';

   try {
      await promisePool.execute(query, [id]);
      res.json({ message: `Successfully deleted the employee with id ${id}` });
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

export default router;