import argon2 from 'argon2';
import { Router } from 'express';
import { validate } from 'express-validation';
import jwt from 'jsonwebtoken';
import type { ResultSetHeader } from 'mysql2';
import { promisePool } from '../config/db-config';
import type { User } from './model_types/types';
import { loginValidator, registerValidator } from './request_validations/auth-validator';

const router = Router();

router.post('/login', validate(loginValidator, {}, {}), async (req, res, next) => {
   const { username, password } = req.body;

   const query = 'SELECT * FROM `admin` WHERE `username` = ?';
   const placeholderValues = [username]

   try {
      const [results] = await promisePool.execute<User[]>(query, placeholderValues);

      // check if the user is available
      const user = results[0];
      if (!user) {
         return res.status(404).json({ message: 'Invalid Username or Password!' });
      }

      // verify password hashes
      const valid = await argon2.verify(user.password, password);
      if (!valid) {
         return res.status(404).json({ message: 'Invalid Username or Password!' });
      }

      // prepare payload for token
      const payload = {
         user: {
            id: user.id,
            username: user.username
         }
      }

      // sign the token
      const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET as jwt.Secret, { expiresIn: '2h' });

      res.json({ accessToken });
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

router.post('/register', validate(registerValidator, {}, {}), async (req, res, next) => {
   const { username, password } = req.body;

   try {
      // Check if another user with the same username is available
      const validationQuery = 'SELECT * FROM `admin` WHERE `username` = ?';
      const [rows] = await promisePool.execute<User[]>(validationQuery, [username]);
      // If available send error
      if (rows[0]) {
         return res.status(400).json({ message: "An user with this username is already available" });
      }

      // hash the password before storing
      const hash = await argon2.hash(password);

      const query = 'INSERT INTO `admin` (`username`, `password`) VALUES (?, ?)';
      const placeholderValues = [username, hash]

      const [result] = await promisePool.execute<ResultSetHeader>(query, placeholderValues);
      res.json({ message: `Successfully created a new user with id ${result.insertId}` });
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

export default router;