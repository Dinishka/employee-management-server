import { Router } from 'express';
import { promisePool } from '../config/db-config';
import { validate } from 'express-validation';
import { createDepartmentValidator, getDepartmentValidator } from './request_validations/department-validator';
import type { ResultSetHeader } from 'mysql2';

const router = Router();

router.get('/', async (_, res, next) => {
   const query = 'SELECT * FROM `department`';

   try {
      const [rows] = await promisePool.execute(query);
      res.json(rows);
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

router.post('/', validate(createDepartmentValidator, {}, {}), async (req, res, next) => {
   const { department_name } = req.body;

   const query = 'INSERT INTO `department` (`department_name`) VALUES (?)';

   try {
      const [result] = await promisePool.execute<ResultSetHeader>(query, [department_name]);
      res.json({ message: `Successfully created a new department with id ${result.insertId}` });
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

router.get('/:id/designations', validate(getDepartmentValidator, {}, {}), async (req, res, next) => {
   const { id } = req.params;

   const query = 'SELECT * FROM `designation` WHERE department_id = ?';

   try {
      const [rows] = await promisePool.execute(query, [id]);
      res.json(rows);
   } catch (e) {
      // pass errors to the global error handler
      next(e);
   }
});

export default router;