import { Joi } from 'express-validation';
import type { schema } from 'express-validation';

const getDepartmentValidator: schema = {
   params: Joi.object({
      id: Joi.number().integer().min(1).required()
   })
}

const createDepartmentValidator: schema = {
   body: Joi.object({
      department_name: Joi.string().trim().max(45).required()
   })
}

export { getDepartmentValidator, createDepartmentValidator };