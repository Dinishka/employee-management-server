import { Joi } from 'express-validation';
import type { schema } from 'express-validation';

const getEmployeeValidator: schema = {
   params: Joi.object({
      id: Joi.number().integer().min(1).required()
   })
}

const createEmployeeValidator: schema = {
   body: Joi.object({
      first_name: Joi.string().trim().max(45).required(),
      last_name: Joi.string().trim().max(45).required(),
      birthday: Joi.date().required(),
      addresses: Joi.array().items(Joi.string().required()).required(),
      contact_numbers: Joi.array().items(Joi.string().required()).required(),
      nic: Joi.string().trim().max(12).required(),
      designation_id: Joi.number().integer().min(1).required()
   })
}

const updateEmployeeValidator: schema = {
   params: Joi.object({
      id: Joi.number().integer().min(1).required()
   }),
   body: Joi.object({
      first_name: Joi.string().trim().max(45).required(),
      last_name: Joi.string().trim().max(45).required(),
      birthday: Joi.date().required(),
      addresses: Joi.array().items(Joi.string().required()).required(),
      contact_numbers: Joi.array().items(Joi.string().required()).required(),
      nic: Joi.string().trim().max(12).required(),
      designation_id: Joi.number().integer().min(1).required()
   })
}

export { getEmployeeValidator, createEmployeeValidator, updateEmployeeValidator };