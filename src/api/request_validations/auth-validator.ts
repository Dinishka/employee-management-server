import { Joi } from 'express-validation';
import type { schema } from 'express-validation';

const loginValidator: schema = {
   body: Joi.object({
      username: Joi.string().trim().max(45).required(),
      password: Joi.string().trim().max(255).required()
   })
}

const registerValidator: schema = {
   body: Joi.object({
      username: Joi.string().trim().max(45).required(),
      password: Joi.string().trim().max(255).required(),
      confirmPassword: Joi.string().required().valid(Joi.ref('password'))
   })
}

export { loginValidator, registerValidator };