import { Joi } from 'express-validation';
import type { schema } from 'express-validation';

const createDesignationValidator: schema = {
   body: Joi.object({
      designation: Joi.string().trim().max(45).required(),
      department_id: Joi.number().integer().min(1).required()
   })
}

export { createDesignationValidator };