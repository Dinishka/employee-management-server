# Introduction

This is a simple REST API for managing employees.

## Authorization

All API requests except `/auth/login` & `/auth/register` require a Bearer token in the `Authorization` header.

```http
Authorization: Bearer `token`
```

## REST API

The REST API to the employee management app is described below.

### Register

#### Request
```http
POST /api/auth/register
```

| Body | Type | Description |
| :--- | :--- | :--- |
| `username` | `string` | **Required**. |
| `password` | `string` | **Required**. |
| `confirmPassword` | `string` | **Required**. |

#### Response
```typescript
{ 
   "message": "Successfully created a new user with id `id`" 
}
```

### Login

#### Request
```http
POST /api/auth/login
```

| Body | Type | Description |
| :--- | :--- | :--- |
| `username` | `string` | **Required**. |
| `password` | `string` | **Required**. |

#### Response
```typescript
{
   "accessToken": string
}
```

### Get all Employess

#### Request
```http
GET /api/employee/
```

#### Response
```typescript
[
   {
      "id": number,
      "first_name": string,
      "last_name": string,
      "birthday": string,
      "addresses": string[],
      "contact_numbers": string[],
      "nic": string,
      "designation_id": number,
      "designation": string,
      "department_id": number,
      "department_name": string
  }
]
```
### Get Employee by ID

#### Request
```http
GET /api/employee/:id
```
| Parameter | Type | Description |
| :--- | :--- | :--- |
| `id` | `string` or `number` | **Required**. Employee's ID |

#### Response
```typescript
{
   "id": number,
   "first_name": string,
   "last_name": string,
   "birthday": string,
   "addresses": string[],
   "contact_numbers": string[],
   "nic": string,
   "designation_id": number,
   "designation": string,
   "department_id": number,
   "department_name": string
}
```

### Create an Employee

#### Request
```http
POST /api/employee
```

| Body | Type | Description |
| :--- | :--- | :--- |
| `first_name` | `string` | **Required**. Employee's first name |
| `last_name` | `string` | **Required**. Employee's last name |
| `birthday` | `string` | **Required**. Employee's birthday |
| `addresses` | `string[]` | **Required**. Employee's addresses |
| `contact_numbers` | `string[]` | **Required**. Employee's contact numbers |
| `nic` | `string` | **Required**. Employee's NIC |
| `designation_id` | `string` or `number` | **Required**. Employee's designation id |

#### Response
```typescript
{ 
   "message": "Successfully created a new employee with id `id`" 
}
```

### Update an Employee

#### Request
```http
PUT /api/employee/:id
```

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `id` | `string` or `number` | **Required**. Employee's ID |

| Body | Type | Description |
| :--- | :--- | :--- |
| `first_name` | `string` | **Required**. Employee's first name |
| `last_name` | `string` | **Required**. Employee's last name |
| `birthday` | `string` | **Required**. Employee's birthday |
| `addresses` | `string[]` | **Required**. Employee's addresses |
| `contact_numbers` | `string[]` | **Required**. Employee's contact numbers |
| `nic` | `string` | **Required**. Employee's NIC |
| `designation_id` | `string` or `number` | **Required**. Employee's designation id |

#### Response
```typescript
{ 
   "message": "Successfully updated the employee" 
}
```

### Delete an Employee

#### Request
```http
DELETE /api/employee/:id
```

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `id` | `string` or `number` | **Required**. Employee's ID |

#### Response
```typescript
{ 
   "message": "Successfully deleted the employee with id `id`" 
}
```

### Get all Departments

#### Request
```http
GET /api/department
```

#### Response
```typescript
[
   {
      "id": number,
      "department_name": string
   }
]
```

### Create a Department

#### Request
```http
POST /api/department
```

| Body | Type | Description |
| :--- | :--- | :--- |
| `department_name` | `string` | **Required**. Name of the Department |

#### Response
```typescript
{ 
   "message": "Successfully created a new department with id `id`" 
}
```

### Get Designations of a Department

#### Request
```http
GET /api/department/:id/designations
```

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `id` | `string` or `number` | **Required**. ID of the Department |

#### Response
```typescript
[
   {
      "id": number,
      "designation": string,
      "department_id": number
   }
]
```

### Create a Designation

#### Request
```http
POST /api/designation
```

| Body | Type | Description |
| :--- | :--- | :--- |
| `designation` | `string` | **Required**. Name of the Designation |
| `department_id` | `number` | **Required**. ID of the Department for the relavent Designation |

#### Response
```typescript
{ 
   "message": "Successfully created a new designation" 
}
```

## Error Responses

All the error responses use the following pattern

```typescript
{
   "message": string,
   "error"?: any
}
```